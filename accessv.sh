#!/bin/bash
arq="access.log"
OC="[1-9]{1,3}"
RE='($OC)\.($OC).($OC)\.($OC)'
if [ -e $arq  ];then
	#grep -Eo $RE $arq | sort -n -u
	sed -r "s/$RE/XXX.\2.\3.XXX/g" < $arq
else
	wget https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log
fi
